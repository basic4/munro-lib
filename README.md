# munro-lib API



---
 Details
- Endpoint:/api/mountain/list
- Description: List All Munros and Munro tops
- Method: GET
- Produces: application/json
  
 Parameters:
- category  	description: optional  values - 'munro' 'top' 
- sortBy    	description: optional  values 'height' 'alhpa'
- sortOrder 	description: optional  values 'asc' 'desc'
- minHeight 	description: minimum height (cannot be greater than maxHeight, if it is provided)
- maxHeight 	description: maximum height (cannot be lesser than minHeight, if it is provided)
- maxRecords 	description: maximum number of records to return

Responses:
- 200: SUCCESS: List of Munros and Munro Tops matching criteria
- 400: BAD REQUEST: bad input parameter

Example: /api/mountain/list?category=top&minHeight=900&maxHeight=950&maxRecords=10&sortBy=height&sortOrder=asc

# Docker Deployment
- Create  Dockerfile
- Add filename to pom.xml
- Configure Dockerfile
- Create image: "docker build -t munro-lib-docker ."
- Run container: "docker run -dp 9090:8080 munro-lib-docker"

These commands will start a container with munro-lib running. 
Munro-lib container will expose port 8080, which will be bound
to port 9090 to local machine.

Example: http://localhost:9090/api/mountain/list?category=top&minHeight=900&maxHeight=1000&maxRecords=100&sortBy=height&sortOrder=desc

# GitLab-CI Steps
- Create project in Gitlab (or import from BitBucket/GitHub)
- Add .gitlab-ci.yml to the root folder of the project (same level as pom.xml)
- Download and Install a Gitlab-Runner: https://docs.gitlab.com/runner/install/
- Good video tutorial on how to set up a runner in Win: https://www.youtube.com/watch?v=2MBhxk2chhM&t=317s
- In case of Java project, a docker image with Java8 and Maven already installed: eg.:image: rvancea/maven-chrome-jdk8
- Register the Gitlab-Runner for the current project: https://docs.gitlab.com/runner/register/index.html

Once all these steps are followed, any commits against the project should trigger a pipeline, described in the .gitlab-ci.yml

