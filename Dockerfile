FROM openjdk:8
EXPOSE 8080
ADD target/munro-lib-docker.jar munro-lib-docker.jar
ENTRYPOINT ["java","-jar","munro-lib-docker.jar"]
