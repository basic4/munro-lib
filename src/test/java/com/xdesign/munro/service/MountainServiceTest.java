package com.xdesign.munro.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.xdesign.munro.entity.CategoryEnum;
import com.xdesign.munro.entity.Filters;
import com.xdesign.munro.entity.Mountain;
import com.xdesign.munro.entity.OrderEnum;
import com.xdesign.munro.entity.SortEnum;
import com.xdesign.munro.repository.MountainRepository;

class MountainServiceTest {
	
	private static final int TOTAL_NO_ENTRIES = 7;
	private static final String HIGHEST_ENTRY = "Cean Chonzie";
	private static final String LOWEST_ENTRY = "Meall nan Eun";
	private static final String ALPHABETICALLY_FIRST = "Aonach Beag";
	private static final String ALPHABETICALLY_LAST = "Zeus Odhair";
	private static final double MAX_HEIGHT = 1100.0;
	private static final double MIN_HEIGHT = 950.0;
	private static final int MAX_RECORDS = 3;
	private MountainService service;
	private List<Mountain> testDataList;
	
	@Mock
	private MountainRepository repo;//= mock(MountainRepository.class);

	@BeforeEach
	void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		this.service = new MountainService();
		this.service.setMountainRepository(repo);
		
		this.testDataList = Arrays.asList(
				new Mountain("Geal-charn", new Double(1200.1), "TOP", "Highest TOP"),
				new Mountain(ALPHABETICALLY_FIRST, new Double(1112.2), "TOP", "2nd Top"),
				new Mountain("Ben Cruachan", new Double(1100), "TOP", "Lowest TOP"),
				new Mountain(HIGHEST_ENTRY, new Double(1500), "MUN", "Highest MUNRO & Highest ENTRY"),
				new Mountain(ALPHABETICALLY_LAST, new Double(1000), "MUN", "2nd MUNRO"),
				new Mountain("Eun nan", new Double(950), "MUN", "3rd MUNRO"),
				new Mountain(LOWEST_ENTRY, new Double(900), "MUN", "Lowest MUNRO & Lowest ENTRY"),
				new Mountain("SHOULD NOT BE LISTED", new Double(10), "", "SHOULD NO BE LISTED")
		);
		
		// Given - set up mock 
		when(repo.findAll()).thenReturn(testDataList);
		
	}
	
	@Test
	void testFilter_combination_1() {
		// Given
		Filters filters = new Filters();
		filters.setMaxHeight(1200.0);
		filters.setMinHeight(900.0);
		filters.setCategory(CategoryEnum.MUN);
		filters.setSortBy(SortEnum.ALPHA);

		// When
		List<Mountain> mountainList = service.fetchList(filters);
		
		//Then
		verify(repo, times(1)).findAll();
		assertThat(mountainList.size(), equalTo(3));
	}
	
	@Test
	void testFilter_combination_2() {
		// Given
		Filters filters = new Filters();
		filters.setMaxHeight(1200.0);
		filters.setMinHeight(900.0);
		filters.setCategory(CategoryEnum.TOP);
		filters.setSortBy(SortEnum.ALPHA);
		filters.setSortOrder(OrderEnum.DESC);

		// When
		List<Mountain> mountainList = service.fetchList(filters);
		
		//Then
		verify(repo, times(1)).findAll();
		assertThat(mountainList.size(), equalTo(2));
	}		


	@Test
	void testFilter_all() {
		// Given
		Filters filters = new Filters();

		// When
		List<Mountain> mountainList = service.fetchList(filters);
		
		//Then
		verify(repo, times(1)).findAll();
		assertThat(mountainList.size(), equalTo(7));
	}		

	@Test
	void testFilter_tops() {
		// Given
		Filters filters = new Filters();
		filters.setCategory(CategoryEnum.TOP);

		// When
		List<Mountain> mountainList = service.fetchList(filters);
		
		//Then
		verify(repo, times(1)).findAll();
		assertThat(mountainList.size(), equalTo(3));
	}	
	
	@Test
	void testFilter_munros() {
		// Given
		Filters filters = new Filters();
		filters.setCategory(CategoryEnum.MUN);

		// When
		List<Mountain> mountainList = service.fetchList(filters);
		
		//Then
		verify(repo, times(1)).findAll();
		assertThat(mountainList.size(), equalTo(4));
	}
	
	@Test
	void testFilter_alhpa_no_order() {
		// Given
		Filters filters = new Filters();
		filters.setSortBy(SortEnum.ALPHA);

		// When
		List<Mountain> mountainList = service.fetchList(filters);
		
		//Then
		verify(repo, times(1)).findAll();
		assertThat(mountainList.get(0).getName(), equalTo(ALPHABETICALLY_FIRST));
	}
	
	@Test
	void testFilter_alhpa_asc() {
		// Given
		Filters filters = new Filters();
		filters.setSortBy(SortEnum.ALPHA);
		filters.setSortOrder(OrderEnum.ASC);

		// When
		List<Mountain> mountainList = service.fetchList(filters);
		
		//Then
		verify(repo, times(1)).findAll();
		assertThat(mountainList.get(0).getName(), equalTo(ALPHABETICALLY_FIRST));
	}
	
	@Test
	void testFilter_alhpa_desc() {
		// Given
		Filters filters = new Filters();
		filters.setSortBy(SortEnum.ALPHA);
		filters.setSortOrder(OrderEnum.DESC);

		// When
		List<Mountain> mountainList = service.fetchList(filters);
		
		//Then
		verify(repo, times(1)).findAll();
		assertThat(mountainList.get(0).getName(), equalTo(ALPHABETICALLY_LAST));
	}	
	
	
	@Test
	void testFilter_hight_asc() {
		// Given
		Filters filters = new Filters();
		filters.setSortBy(SortEnum.HEIGHT);

		// When
		List<Mountain> mountainList = service.fetchList(filters);
		
		//Then
		verify(repo, times(1)).findAll();
		assertThat(mountainList.get(0).getName(), equalTo(LOWEST_ENTRY));
	}
	
	@Test
	void testFilter_hight_desc() {
		// Given
		Filters filters = new Filters();
		filters.setSortBy(SortEnum.HEIGHT);
		filters.setSortOrder(OrderEnum.DESC);

		// When
		List<Mountain> mountainList = service.fetchList(filters);
		
		//Then
		verify(repo, times(1)).findAll();
		assertThat(TOTAL_NO_ENTRIES, equalTo(mountainList.size()));
		assertThat(mountainList.get(0).getName(), equalTo(HIGHEST_ENTRY));
	}	
		
	
	@Test
	void testFilter_mx_records() {
		// Given
		Filters filters = new Filters();
		filters.setMaxRecords(MAX_RECORDS);

		// When
		List<Mountain> mountainList = service.fetchList(filters);
		
		//Then
		verify(repo, times(1)).findAll();
		assertThat(mountainList.size(), equalTo(MAX_RECORDS));
	}	
	
	@Test
	void testFilter_zero_mxrecords() {
		// Given
		Filters filters = new Filters();
		filters.setMaxRecords(0);

		// When
		List<Mountain> mountainList = service.fetchList(filters);
		
		//Then
		verify(repo, times(1)).findAll();
		assertThat(mountainList.size(), equalTo(0));
	}		
	
	@Test
	void testFilter_min_height() {
		// Given
		Filters filters = new Filters();
		filters.setMinHeight(MIN_HEIGHT);

		// When
		List<Mountain> mountainList = service.fetchList(filters);
		
		//Then
		verify(repo, times(1)).findAll();
		assertThat(mountainList.size(), equalTo(6));
	}		
	
	@Test
	void testFilter_max_height() {
		// Given
		Filters filters = new Filters();
		filters.setMaxHeight(MAX_HEIGHT);

		// When
		List<Mountain> mountainList = service.fetchList(filters);
		
		//Then
		verify(repo, times(1)).findAll();
		assertThat(mountainList.size(), equalTo(4));
	}		
	
	
	@Test
	void testFilter_min_max_height() {
		// Given
		Filters filters = new Filters();
		filters.setMaxHeight(MAX_HEIGHT);
		filters.setMinHeight(MIN_HEIGHT);

		// When
		List<Mountain> mountainList = service.fetchList(filters);
		
		//Then
		verify(repo, times(1)).findAll();
		assertThat(mountainList.size(), equalTo(3));
	}	
	
}
