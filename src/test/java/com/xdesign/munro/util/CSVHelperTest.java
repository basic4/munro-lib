package com.xdesign.munro.util;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;

import com.xdesign.munro.entity.Mountain;

class CSVHelperTest {

	@Test
	void testParseCsvFile() throws Exception {
		CSVHelper csvHelper = new CSVHelper();
		List<Mountain> result = csvHelper.parseCsvFile("munrotest_v62.csv");
		assertNotNull(result);
		assertTrue(result.size() > 0);
	}

	@Test
	void testParseCsvFile_fileNotFound()  {
		CSVHelper csvHelper = new CSVHelper();
		assertThrows(
				NullPointerException.class,
	            () -> {csvHelper.parseCsvFile("filedoesntexist.csv");}
	     );
	}
	
	@Test
	void testParseCsvEmptyFile() throws Exception {
		CSVHelper csvHelper = new CSVHelper();
		List<Mountain> result = csvHelper.parseCsvFile("empty.csv");
		assertNotNull(result);
		assertTrue(result.isEmpty());
	}	

	@Test
	void testParseCsvMalformedFile() {
		CSVHelper csvHelper = new CSVHelper();
		assertThrows(
				RuntimeException.class,
	            () -> {csvHelper.parseCsvFile("malformed.csv");}
	     );
	}		
}
