package com.xdesign.munro.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.xdesign.munro.entity.Mountain;
import com.xdesign.munro.service.MountainService;

@WebMvcTest(MountainController.class)
public class MountainControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private MountainService service;	

	private List<Mountain> testDataList;
	
	@BeforeEach
	void setUp() throws Exception {
		
		this.testDataList = Arrays.asList(
				new Mountain("Geal-charn", new Double(1200.1), "TOP", "Highest TOP"),
				new Mountain("Aonach Beag", new Double(1112.2), "TOP", "ALPHABETICALLY_FIRST"),
				new Mountain("Ben Cruachan", new Double(1100), "TOP", "Lowest TOP"),
				new Mountain("Cean Chonzie", new Double(1500), "MUN", "Highest MUNRO & Highest ENTRY"),
				new Mountain("Zeus Odhair", new Double(1000), "MUN", "ALPHABETICALLY_LAST"),
				new Mountain("Eun nan", new Double(950), "MUN", "3rd MUNRO"),
				new Mountain("Meall nan Eun", new Double(900), "MUN", "Lowest MUNRO & Lowest ENTRY")
		);
		
	}	
	
	@Test
	public void testValidation_no_filters() throws Exception {
		
		when(service.fetchList(any())).thenReturn(testDataList);
		
		this.mockMvc.perform(get("/api/mountain/list"))
	        .andExpect(status().isOk())
	        .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
	        .andExpect(jsonPath("$[0].name", is("Geal-charn")))
	        .andExpect(jsonPath("$[0].height", is(1200.1)))
	        .andExpect(jsonPath("$[0].category", is("TOP")))
	        .andExpect(jsonPath("$[6].name", is("Meall nan Eun")))
	        .andExpect(jsonPath("$[6].height", is(900.0)))
	        .andExpect(jsonPath("$[6].category", is("MUN")));

        verify(service, times(1)).fetchList(any());
        verifyNoMoreInteractions(service);		
	}	
	
	
	@Test
	public void testValidation_category_invalid() throws Exception {
		// "/api/mountain/list?category=peak&minHeight=10&maxHeight=10&maxRecords=10&sortBy=height&sortOrder=asc"
		
		when(service.fetchList(any())).thenReturn(testDataList);
		
		this.mockMvc.perform(get("/api/mountain/list?category=peak"))
	        .andExpect(status().is4xxClientError())
	        .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
	        .andExpect(jsonPath("status", is(400)))
	        .andExpect(jsonPath("error", is("typeMismatch: category does not allow value 'peak'")));
		verifyNoInteractions(service);
	}	

	
	@Test
	public void testValidation_sortBy_invalid() throws Exception {
		
		when(service.fetchList(any())).thenReturn(testDataList);
		
		this.mockMvc.perform(get("/api/mountain/list?sortBy=gridReference"))
	        .andExpect(status().is4xxClientError())
	        .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
	        .andExpect(jsonPath("status", is(400)))
	        .andExpect(jsonPath("error", is("typeMismatch: sortBy does not allow value 'gridReference'")));
		verifyNoInteractions(service);
	}	
	@Test
	public void testValidation_orderBy_invalid() throws Exception {
		
		when(service.fetchList(any())).thenReturn(testDataList);
		
		this.mockMvc.perform(get("/api/mountain/list?sortOrder=ascending"))
	        .andExpect(status().is4xxClientError())
	        .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
	        .andExpect(jsonPath("status", is(400)))
	        .andExpect(jsonPath("error", is("typeMismatch: sortOrder does not allow value 'ascending'")));
		verifyNoInteractions(service);
	}	
	
	
	@Test
	public void testValidation_maxRecords_invalid_character() throws Exception {
		
		when(service.fetchList(any())).thenReturn(testDataList);
		
		this.mockMvc.perform(get("/api/mountain/list?maxRecords=££"))
	        .andExpect(status().is4xxClientError())
	        .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
	        .andExpect(jsonPath("status", is(400)))
	        .andExpect(jsonPath("error", is("typeMismatch: maxRecords does not allow value '££'")));
		verifyNoInteractions(service);
	}
	@Test
	public void testValidation_maxRecords_negative() throws Exception {
		
		when(service.fetchList(any())).thenReturn(testDataList);
		
		this.mockMvc.perform(get("/api/mountain/list?maxRecords=-1"))
	        .andExpect(status().is4xxClientError())
	        .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
	        .andExpect(jsonPath("status", is(400)))
	        .andExpect(jsonPath("error", is("maxRecords: Minimum value is 0")));
		verifyNoInteractions(service);
	}	
	
	
	@Test
	public void testValidation_minHeigth_invalid_character() throws Exception {
		
		when(service.fetchList(any())).thenReturn(testDataList);
		
		this.mockMvc.perform(get("/api/mountain/list?minHeight=**"))
	        .andExpect(status().is4xxClientError())
	        .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
	        .andExpect(jsonPath("status", is(400)))
	        .andExpect(jsonPath("error", is("typeMismatch: minHeight does not allow value '**'")));
		verifyNoInteractions(service);
	}		
	@Test
	public void testValidation_minHeigth_negative() throws Exception {
		
		when(service.fetchList(any())).thenReturn(testDataList);
		
		this.mockMvc.perform(get("/api/mountain/list?minHeight=-1"))
	        .andExpect(status().is4xxClientError())
	        .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
	        .andExpect(jsonPath("status", is(400)))
	        .andExpect(jsonPath("error", is("minHeight: Minimum value is 0")));
		verifyNoInteractions(service);
	}	
	@Test
	public void testValidation_minHeigth_greaterThan_maxHeight() throws Exception {
		
		when(service.fetchList(any())).thenReturn(testDataList);
		
		this.mockMvc.perform(get("/api/mountain/list?minHeight=1205.6&maxHeight=900.1"))
	        .andExpect(status().is4xxClientError())
	        .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
	        .andExpect(jsonPath("status", is(400)))
	        .andExpect(jsonPath("error", is("maxHeight(900.1) must be equal to or greater than minHeight (1205.6)")));
		verifyNoInteractions(service);
	}	
	
	
	@Test
	public void testValidation_maxHeight_invalid_character() throws Exception {
		
		when(service.fetchList(any())).thenReturn(testDataList);
		
		this.mockMvc.perform(get("/api/mountain/list?maxHeight=J"))
	        .andExpect(status().is4xxClientError())
	        .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
	        .andExpect(jsonPath("status", is(400)))
	        .andExpect(jsonPath("error", is("typeMismatch: maxHeight does not allow value 'J'")));
		verifyNoInteractions(service);
	}		
	@Test
	public void testValidation_maxHeight_negative() throws Exception {
		
		when(service.fetchList(any())).thenReturn(testDataList);
		
		this.mockMvc.perform(get("/api/mountain/list?maxHeight=-1"))
	        .andExpect(status().is4xxClientError())
	        .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
	        .andExpect(jsonPath("status", is(400)))
	        .andExpect(jsonPath("error", is("maxHeight: Minimum value is 0")));
		verifyNoInteractions(service);
	}	
}
