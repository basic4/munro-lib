package com.xdesign.munro.controller;

import java.util.List;

import javax.validation.ConstraintViolationException;
import javax.validation.constraints.Min;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import com.xdesign.munro.entity.CategoryEnum;
import com.xdesign.munro.entity.ErrorResponse;
import com.xdesign.munro.entity.Filters;
import com.xdesign.munro.entity.Mountain;
import com.xdesign.munro.entity.OrderEnum;
import com.xdesign.munro.entity.SortEnum;
import com.xdesign.munro.exceptions.FilterHeightRangeException;
import com.xdesign.munro.service.MountainService;

@RestController
@Validated
public class MountainController {
	private static final Logger LOG = LoggerFactory.getLogger(MountainController.class);
	
	@Autowired
	MountainService service;
	
	
	@RequestMapping("/health")
	public String healthCheck() {
		return "healthy";
	}
	
	@GetMapping(value = "/api/mountain/list", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<List<Mountain>> listFilters(
			@RequestParam(required = false) CategoryEnum category,
			@RequestParam(required = false) SortEnum sortBy,
			@RequestParam(required = false) OrderEnum sortOrder,
			@RequestParam(required = false) @Min(value=0,message="Minimum value is 0") Integer maxRecords,
			@RequestParam(required = false) @Min(value=0,message="Minimum value is 0") Double minHeight,
			@RequestParam(required = false) @Min(value=0,message="Minimum value is 0") Double maxHeight
			) throws FilterHeightRangeException {
		
		LOG.info("Request :/api/mountain/list | Filters: category={},sortBy={},sortOrder={},maxRecords={},minHeight={},maxHeight={}",category,sortBy, sortOrder, maxRecords, minHeight, maxHeight);
		Filters filters = new Filters(category,sortBy, sortOrder, maxRecords, minHeight, maxHeight);
		List<Mountain> mountainList = service.fetchList(filters);
		LOG.info("Result size={}",mountainList.size());
		return new ResponseEntity<>(mountainList,HttpStatus.OK);
	    
	}	
	
    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<String> onValidationError(ConstraintViolationException ex) {   
    	ErrorResponse errorResponse = new ErrorResponse(HttpStatus.BAD_REQUEST.value(),ex.getMessage());    	
    	HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Content-Type", MediaType.APPLICATION_JSON_VALUE.toString());
        LOG.warn("ConstraintViolation Exception:{}",errorResponse.getError());
        return ResponseEntity.badRequest().headers(responseHeaders).body(errorResponse.toString());
    }
    
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<String> onConversionError(MethodArgumentTypeMismatchException ex) { 
    	String errorMessage = ex.getErrorCode()+": "+ex.getName()+" does not allow value '"+ex.getValue()+"'";    	
    	ErrorResponse errorResponse = new ErrorResponse(HttpStatus.BAD_REQUEST.value(),errorMessage);  
    	HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Content-Type", MediaType.APPLICATION_JSON_VALUE.toString());
        LOG.warn("MethodArgumentTypeMismatch Exception:{}",errorResponse.getError());
        return ResponseEntity.badRequest().headers(responseHeaders).body(errorResponse.toString());
    }
    
    @ExceptionHandler(FilterHeightRangeException.class)
    public ResponseEntity<String> onHeightRangeError(FilterHeightRangeException ex) {
    	String errorMessage = "maxHeight("+ex.getMaxHeight()+") must be equal to or greater than minHeight ("+ex.getMinHeight()+")";    	
    	ErrorResponse errorResponse = new ErrorResponse(HttpStatus.BAD_REQUEST.value(),errorMessage);    	
    	HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Content-Type", MediaType.APPLICATION_JSON_VALUE.toString());
        LOG.warn("FilterHeightRange Exception:{}",errorResponse.getError());
        return ResponseEntity.badRequest().headers(responseHeaders).body(errorResponse.toString());
    }     
}
