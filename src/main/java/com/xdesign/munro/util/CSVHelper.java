package com.xdesign.munro.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.CsvToBeanFilter;
import com.xdesign.munro.entity.Mountain;

public class CSVHelper {
	
	private static String finalename = "munrotab_v62.csv";
    
    public List<Mountain> parseCsvFile() throws Exception {
       return parseCsvFile(finalename);
    }  
    
    public List<Mountain> parseCsvFile(String name) throws Exception {
        InputStream fis = CSVHelper.class.getClassLoader().getResourceAsStream(name);
        BufferedReader reader = new BufferedReader(new InputStreamReader(fis, StandardCharsets.UTF_8));
        List<Mountain> mountainList =  buildListOfBeans(reader); 
        reader.close();
        return mountainList;
     }       
    
    private List<Mountain> buildListOfBeans(BufferedReader reader) throws Exception {
        ColumnPositionMappingStrategy mappingStrategy = new ColumnPositionMappingStrategy();
        mappingStrategy.setType(Mountain.class);
        CsvToBeanFilter filter = new EmptyLineFilter();
        
        List<Mountain> retList = new ArrayList<Mountain>();
    	CsvToBean<Mountain> cb = new CsvToBeanBuilder<Mountain>(reader)
    		.withMappingStrategy(mappingStrategy)
    		.withFilter(filter)
    		.withSkipLines(1)
    		.build();

 		retList = cb.parse();
     	return retList;
   }   
    
    private class EmptyLineFilter implements CsvToBeanFilter {
     	public boolean allowLine(String[] line) {
     		return !line[0].isEmpty();
        }
     }
}
