package com.xdesign.munro.entity;

public enum SortEnum {
	ALPHA("ALPHA"),
	alpha("alpha"),	
	HEIGHT("HEIGHT"),
	height("height");

	private String value;

	SortEnum(String value) {
		this.value=value;
	}
	
	public String getValue() {
		return value;
	}

}
