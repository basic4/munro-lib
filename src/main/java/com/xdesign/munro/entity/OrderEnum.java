package com.xdesign.munro.entity;

public enum OrderEnum {
	ASC("ASC"),
	asc("asc"),	
	DESC("DESC"),
	desc("desc");

	private String value;

	OrderEnum(String value) {
		this.value=value;
	}
	
	public String getValue() {
		return value;
	}

}
