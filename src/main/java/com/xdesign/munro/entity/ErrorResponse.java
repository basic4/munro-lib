package com.xdesign.munro.entity;

import java.util.Date;

public class ErrorResponse {
	
	private int status;
	private String error;
	private Date timestamp;
	
	public ErrorResponse(int status, String error) {
		super();
		this.status = status;
		this.error = error.replace("\"", "'").replace("listFilters.", "");
		this.timestamp = new Date();
	}
	
	public int getStatus() {
		return status;
	}

	public String getError() {
		return error;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("{\n");
		sb.append(" 	\"status\":"+status+",").append("\n");
		sb.append(" 	\"error\":\""+error+"\",").append("\n");
		sb.append(" 	\"timestamp\":\""+timestamp+"\"").append("\n");
		sb.append("}");
		return sb.toString();
	}
}
