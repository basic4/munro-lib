package com.xdesign.munro.entity;

public enum CategoryEnum {
	MUN("MUN"),
	mun("mun"),	
	TOP("TOP"),
	top("top");

	private String value;

	CategoryEnum(String value) {
		this.value=value;
	}
	
	public String getValue() {
		return value;
	}

}
