package com.xdesign.munro.entity;

import org.springframework.stereotype.Component;

import com.opencsv.bean.CsvBindByPosition;

@Component
public class Mountain {
	
	public Mountain(String name, Double height, String category, String gridReference) {
		super();
		this.name = name;
		this.height = height;
		this.category = category;
		this.gridReference = gridReference;
	}	
	
	public Mountain() {}
	
	@CsvBindByPosition(position = 5)
	private String name;
	
	@CsvBindByPosition(position = 9)
	private Double height;
	
	@CsvBindByPosition(position = 13)
	private String gridReference;
	
	@CsvBindByPosition(position = 27)
	private String category	;
	
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getHeight() {
		return height;
	}
	public void setHeight(Double height) {
		this.height = height;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getGridReference() {
		return gridReference;
	}
	public void setGridReference(String gridReference) {
		this.gridReference = gridReference;
	}

	public String toString() {
		return this.getName()+" ("+this.getHeight()+")";
	}
	
	
}
