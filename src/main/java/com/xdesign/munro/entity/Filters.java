package com.xdesign.munro.entity;

import com.xdesign.munro.exceptions.FilterHeightRangeException;

public class Filters {

	private CategoryEnum category = null;
	private SortEnum sortBy;
	private OrderEnum sortOrder;
	
	private Integer maxRecords;
	
	private Double minHeight;
	private Double maxHeight;
	
	public Filters() {};
	
	public Filters(CategoryEnum category, SortEnum sortBy, OrderEnum sortOrder, Integer maxRecords, Double minHeight,
			Double maxHeight) throws FilterHeightRangeException {
		super();
		this.category = category;
		this.sortBy = sortBy;
		this.sortOrder = sortOrder;
		this.maxRecords = maxRecords;
		this.minHeight = minHeight;
		this.maxHeight = maxHeight;
		
		validateHeightRange();
	}

	private void validateHeightRange() throws FilterHeightRangeException {
		if (minHeight!=null && maxHeight!=null) {
			if (minHeight>maxHeight) {
				throw new FilterHeightRangeException(minHeight, maxHeight);
			}
		}
	}

	public CategoryEnum getCategory() {
		return category;
	}

	public void setCategory(CategoryEnum category) {
		this.category = category;
	}

	public SortEnum getSortBy() {
		return sortBy;
	}

	public void setSortBy(SortEnum sortBy) {
		this.sortBy = sortBy;
	}

	public OrderEnum getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(OrderEnum sortOrder) {
		this.sortOrder = sortOrder;
	}

	public Integer getMaxRecords() {
		return maxRecords;
	}

	public void setMaxRecords(Integer maxRecords) {
		this.maxRecords = maxRecords;
	}

	public Double getMinHeight() {
		return minHeight;
	}

	public void setMinHeight(Double minHeight) {
		this.minHeight = minHeight;
	}

	public Double getMaxHeight() {
		return maxHeight;
	}

	public void setMaxHeight(Double maxHeight) {
		this.maxHeight = maxHeight;
	}
	
}
