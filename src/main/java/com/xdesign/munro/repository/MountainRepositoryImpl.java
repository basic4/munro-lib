package com.xdesign.munro.repository;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.xdesign.munro.entity.Mountain;
import com.xdesign.munro.util.CSVHelper;

@Repository
public class MountainRepositoryImpl implements MountainRepository {
	
	private static final Logger LOG = LoggerFactory.getLogger(MountainRepositoryImpl.class);
	private List<Mountain> list = null;
	
	@PostConstruct
	public void runAfterObjectCreated() {
		LOG.info("Initialising Repository");
		CSVHelper csvHelper = new CSVHelper();
		try {
			list = csvHelper.parseCsvFile();
		} catch (Exception e) {
			LOG.error("Respository initialisation failed: "+ e.getCause().getMessage());
			list = new ArrayList<Mountain>();
		}
	}	

	public List<Mountain> findAll() {
		return list;
	}

}
