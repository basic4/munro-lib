package com.xdesign.munro.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.xdesign.munro.entity.Mountain;

@Repository
public interface MountainRepository {
	
	List<Mountain> findAll();
	
}
