package com.xdesign.munro.exceptions;

public class FilterHeightRangeException extends Exception {

	public FilterHeightRangeException(String message) {
		super(message);
	}

	public FilterHeightRangeException(Double minHeight, Double maxHeight) {
		super();
		this.minHeight = minHeight;
		this.maxHeight = maxHeight;
	}

	private static final long serialVersionUID = -8890571922739521862L;
	private Double minHeight;
	private Double maxHeight;
	
	public Double getMinHeight() {
		return minHeight;
	}
	public Double getMaxHeight() {
		return maxHeight;
	}

	
}
