package com.xdesign.munro.service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xdesign.munro.entity.Filters;
import com.xdesign.munro.entity.Mountain;
import com.xdesign.munro.entity.OrderEnum;
import com.xdesign.munro.entity.SortEnum;
import com.xdesign.munro.repository.MountainRepository;

@Service
public class MountainService{
	private static final Logger LOG = LoggerFactory.getLogger(MountainService.class);
	
	@Autowired
	private MountainRepository mountainRepository;
	
	public void setMountainRepository(MountainRepository mountainRepository) {
		this.mountainRepository = mountainRepository;
	}		

	public List<Mountain> fetchList(Filters filters) {
		LOG.debug("Retrieving data from repository");
		
		List<Mountain> list =  mountainRepository.findAll();		
		Stream<Mountain> stream = list.stream();
		
		LOG.debug("Applying filters");
		/* Sort By and Sort Order */
		if (filters.getSortBy()!=null && !filters.getSortBy().getValue().isEmpty()) {
			
			if (filters.getSortBy().equals(SortEnum.HEIGHT) || filters.getSortBy().equals(SortEnum.height)) {
				stream = stream.sorted(Comparator.comparing(Mountain::getHeight));
				if (filters.getSortOrder() != null && (filters.getSortOrder().equals(OrderEnum.DESC) || filters.getSortOrder().equals(OrderEnum.desc))) {
					List<Mountain> lst = stream.collect(Collectors.toList());
					Collections.reverse(lst);
					stream = lst.stream();
				}				
			} else if (filters.getSortBy().equals(SortEnum.ALPHA) || filters.getSortBy().equals(SortEnum.alpha)) {
				stream = stream.sorted(Comparator.comparing(Mountain::getName));
				if (filters.getSortOrder() != null && (filters.getSortOrder().equals(OrderEnum.DESC) || filters.getSortOrder().equals(OrderEnum.desc))) {
					List<Mountain> lst = stream.collect(Collectors.toList());
					Collections.reverse(lst);
					stream = lst.stream();
				}				
				
			}
		}
		/* Filter by Category */
		if (filters.getCategory() != null && !filters.getCategory().getValue().isEmpty()) {
			stream = stream.filter(( (m) -> m.getCategory().equalsIgnoreCase(filters.getCategory().getValue()) ));
		} else {
			/* Filters out entries with empty category*/
			stream = stream.filter(( (m) -> !m.getCategory().isEmpty() ));
		}		

		/* Filter by MinHeight */
		if (filters.getMinHeight() != null) {
			stream = stream.filter((m) ->  m.getHeight() >= filters.getMinHeight());
		} 		
		/* Filter by MaxHeight */
		if (filters.getMaxHeight()  != null) {
			stream = stream.filter((m) ->  m.getHeight() <= filters.getMaxHeight());
		} 
		
		/* Filter by MaxRecords */
		if (filters.getMaxRecords()  != null) {
			stream = stream.limit(filters.getMaxRecords());
		} 	
			
		LOG.debug("Returning stream as List");
		return stream.collect(Collectors.toList());
	}
}
